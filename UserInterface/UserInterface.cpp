#include "UserInterface.h"

using namespace std;

int UserInterface::currentCardNumber = 0;

void UserInterface::initUI() {
    vector<GraphicsCard> allCards = Utils::getDetailsArrayFromFile();

    UserInterface::showUI(allCards, 0);
}

void UserInterface::showUI(vector<GraphicsCard> &allCards, int numberToShow) {
    UserInterface::showGraphicsCardTable(allCards[numberToShow]);
    UserInterface::showMenu(allCards);
}

void UserInterface::showMenu(vector<GraphicsCard> &allCards) {
    string userInput = "";

    cout << "1: Show Previous; 2: Show Next; 3: Create New; 4: Delete Current;" << endl;
    cout << "5: Find; 9: Exit." << endl;
    cin >> userInput;

    switch (stoi(userInput)) {
        case 1:
            if (UserInterface::currentCardNumber > 0) {
                UserInterface::currentCardNumber--;
            }
            UserInterface::showUI(allCards, UserInterface::currentCardNumber);
            break;
        case 2:
            if (UserInterface::currentCardNumber < allCards.size() - 1) {
                UserInterface::currentCardNumber++;
            }
            UserInterface::showUI(allCards, UserInterface::currentCardNumber);
            break;
        case 3:
            UserInterface::createCard(allCards);
            UserInterface::saveChanges(allCards);
            UserInterface::currentCardNumber = (int)allCards.size() - 1;
            UserInterface::showUI(allCards, UserInterface::currentCardNumber);
            break;
        case 4:
            allCards.erase(allCards.begin() + currentCardNumber);
            UserInterface::saveChanges(allCards);
            if (allCards.size() > 0) {
                UserInterface::showUI(allCards, 0);
            } else {
                cout << "No data to show." << endl;
                UserInterface::showMenu(allCards);
            }
            break;
        case 5:
            UserInterface::find(allCards);
            UserInterface::showMenu(allCards);
            break;
        case 9:
            break;
        default:
            UserInterface::showUI(allCards, UserInterface::currentCardNumber);
            break;
    }
}

void UserInterface::find(vector<GraphicsCard> &allCards) {
    string searchString;
    size_t found;

    cout << "Please enter the search string: " << flush;
    cin >> searchString;

    for (int i = 0; i < allCards.size(); i++) {
        found = allCards[i].getParameters()[1].find(searchString);
        if (found != std::string::npos) {
            showGraphicsCardTable(allCards[i]);
            UserInterface::currentCardNumber = i;
        }
    }
}

void UserInterface::createCard(vector<GraphicsCard> &allCards) {
    string detailInfo[9] = {};
    unsigned counter = 0;
    vector<string> parameters = GraphicsCard::getParameterNames();

    cout << "Please enter card details:" << endl;
    for (string parameter : parameters) {
        if (parameter == "Type") {
            continue;

        }
        cout << parameter << ": " << flush;
        cin >> detailInfo[counter];
        counter++;
    }
    allCards.push_back(GraphicsCard(
        "Graphics Card",
        detailInfo[0],
        stoi(detailInfo[1]),
        stoi(detailInfo[2]),
        stoi(detailInfo[3]),
        stoi(detailInfo[4]),
        stoi(detailInfo[5]),
        detailInfo[6],
        detailInfo[7],
        detailInfo[8]
    ));
}

int UserInterface::findLongestString(string stringArray[],
                                     int elemNum) {
    int maxLength = 0;

    for (unsigned int i = 0; i < elemNum; i++) {
        if (stringArray[i].length() > maxLength) {
            maxLength = (int)stringArray[i].length();
        }
    }
    return maxLength;
}

void UserInterface::padTo(string &str, int num, char paddingChar) {
    if(num > str.size()) {
        str.insert(str.size(), num - str.size(), paddingChar);
    }
}

void UserInterface::showGraphicsCardTable(GraphicsCard &card) {
    vector<string> paramNames = GraphicsCard::getParameterNames();
    vector<string> parameters = card.getParameters();

    const int serviceSymbolsNumber = 7; // Количество служебных символов для формирования таблицы
    // Включая интервалы между ячейками и отступы внутри ячейки
    int longestParameterName = UserInterface::findLongestString(
            &paramNames[0],
            (int)paramNames.size()
    );
    int longestParameter = UserInterface::findLongestString(
            &parameters[0],
            (int)paramNames.size()
    );
    int totalSymbolNumber = serviceSymbolsNumber + longestParameter + longestParameterName;
    string rowSeparator((unsigned int)totalSymbolNumber, '*');

    cout << rowSeparator << endl;
    for (int i = 0; i < (int)paramNames.size(); i++) {
        UserInterface::padTo(parameters[i], longestParameter, ' ');
        cout << "* "
             << setw(longestParameterName)
             << paramNames[i]
             << " * "
             << parameters[i]
             <<" *"
             << endl;
        cout << rowSeparator << endl;
    }
}

void UserInterface::saveChanges(vector<GraphicsCard> &allCards) {
    Utils::deleteFile();
    for (GraphicsCard card : allCards) {
        Utils::writeDetailToFile(card);
    }
}