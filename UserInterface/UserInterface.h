#ifndef INC_2_USERINTERFACE_H
#define INC_2_USERINTERFACE_H

#include <iostream>
#include <sstream>
#include <vector>
#include <iomanip>

#include "../Utils/Utils.h"
#include "../Detail/Detail.h"
#include "../Detail/GraphicsCard/GraphicsCard.h"

class UserInterface {
public:
    static int currentCardNumber;
    static void initUI();
    static void showUI(vector<GraphicsCard> &allCards, int numberToShow);
    static void showMenu(vector<GraphicsCard> &allCards);
    static void showGraphicsCardTable(GraphicsCard &card);
    static void padTo(string &str, int num, char paddingChar);
    static int findLongestString(string stringArray[],
                                 int elemNum);
    static void saveChanges(vector<GraphicsCard> &allCards);
    static void createCard(vector<GraphicsCard> &allCards);
    static void find(vector<GraphicsCard> &allCards);
};


#endif //INC_2_USERINTERFACE_H
