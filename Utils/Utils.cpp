#include <vector>
#include <algorithm>
#include <string>
#include "Utils.h"

using namespace std;

const string Utils::detailsFileName = "details.txt";

void Utils::compareDetailsByName(Detail &detailOne, Detail &detailTwo) {
    if (detailOne.getName() == detailTwo.getName()) {
        cout << "Details have the same name" << endl;
    } else {
        cout << "Details have different names" << endl;
    }
}

void Utils::compareDetailsByYear(Detail &detailOne, Detail &detailTwo) {
    int year1 = detailOne.getYear();
    int year2 = detailTwo.getYear();

    if (year1 < year2) {
        cout << "The first detail is older" << endl;
    } else if (year1 > year2) {
        cout << "The second detail is older" << endl;
    } else {
        cout << "The details were produced at the same year" << endl;
    }
}

void Utils::writeDetailToFile(Detail &detail) {
    ofstream detailsFile(detailsFileName, std::ios::app);

    if (detailsFile.is_open()) {
        cout << "-------------------------------------------" << endl;
        cout << "Writing to file: " << detailsFileName << endl;
        cout << "-------------------------------------------" << endl;

        cout << detail.getFormattedParams().str();
        detailsFile << detail.getFormattedParams().str();
        detailsFile << endl;
        detailsFile.close();

        cout << "-------------------------------------------" << endl;
        cout << "Successfully wrote to file: " << detailsFileName << endl;
        cout << "-------------------------------------------" << endl;
    } else {
        cout << "Unable to open file";
    }
}

void Utils::printDetailsFromFile() {
    string line;

    ifstream detailsFile(detailsFileName, ios::app);

    if (detailsFile.is_open()) {
        cout << "-------------------------------------------" << endl;
        cout << "Reading from file:" << detailsFileName << endl;
        cout << "-------------------------------------------" << endl;

        while (getline(detailsFile, line)) {
            cout << line << '\n';
        }
        detailsFile.close();
        cout << "-------------------------------------------" << endl;
        cout << "Successfully read from file: " << detailsFileName << endl;
        cout << "-------------------------------------------" << endl;
    } else {
        cout << "Unable to open file";
    }
}

vector<GraphicsCard> Utils::getDetailsArrayFromFile() {
    vector<GraphicsCard> allDetails;
    string line;
    string detailInfo[10] = {};
    int counter = 0;

    ifstream detailsFile(detailsFileName, ios::app);

    if (detailsFile.is_open()) {
        cout << "-------------------------------------------" << endl;
        cout << "Reading from file:" << detailsFileName << endl;
        cout << "-------------------------------------------" << endl;

        while (getline(detailsFile, line)) {
            if (line != "") {
                string fixedLine(find(line.begin(), line.end(), ':') + 2, line.end());
                detailInfo[counter] = fixedLine;
                counter++;
            } else {
                counter = 0;
                allDetails.push_back(GraphicsCard(
                    detailInfo[0],
                    detailInfo[1],
                    stoi(detailInfo[2]),
                    stoi(detailInfo[3]),
                    stoi(detailInfo[4]),
                    stoi(detailInfo[5]),
                    stoi(detailInfo[6]),
                    detailInfo[7],
                    detailInfo[8],
                    detailInfo[9]
                ));
            }
        }
        detailsFile.close();

        cout << "-------------------------------------------" << endl;
        cout << "Successfully read from file: " << detailsFileName << endl;
        cout << "-------------------------------------------" << endl;
    } else {
        cout << "Unable to open file";
    }

    return allDetails;
}

void Utils::deleteFile() {
    remove(Utils::detailsFileName.c_str());
}

void Utils::CreateTestData() {
    Utils::deleteFile();

    Detail detail1("Motherboard", "ASUS A68HM-K", 2015, 567, 50, 226, 180);
    Detail detail2("Motherboard", "ASUS A68HM-K", 2016, 567, 50, 226, 180);
    Detail detail3("Motherboard", "MSI AM1M", 2015, 567, 50, 226, 180);

    GraphicsCard graphicsCard1("Graphics card",
                               "ASUS GeForce 210 [210-SL-TC1GD3-L]",
                               2015,
                               567,
                               50,
                               226,
                               180,
                               "10.1",
                               "3.2",
                               "PCI-E 2.0"
    );
    GraphicsCard graphicsCard2("Graphics card",
                               "ASUS AMD Radeon HD5450 [EAH5450 SILENT/DI/1GD3(LP)]",
                               2015,
                               567,
                               50,
                               226,
                               180,
                               "11",
                               "3.2",
                               "PCI-E 2.1");

    cout << detail1.getFormattedParams().str();
    cout << detail2.getFormattedParams().str();
    cout << detail3.getFormattedParams().str();

    cout << graphicsCard1.getFormattedParams().str();
    cout << graphicsCard2.getFormattedParams().str();

    Utils::compareDetailsByName(detail1, detail2);
    Utils::compareDetailsByYear(detail1, detail2);
    Utils::compareDetailsByYear(detail2, detail3);
    Utils::compareDetailsByYear(detail1, detail3);

    Utils::writeDetailToFile(graphicsCard1);
    Utils::writeDetailToFile(graphicsCard2);

    cout << endl;
    Utils::printDetailsFromFile();

    Utils::getDetailsArrayFromFile();
}