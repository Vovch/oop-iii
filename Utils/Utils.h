#ifndef INC_2_UTILS_H
#define INC_2_UTILS_H

#include <iostream>
#include <fstream>
#include "../Detail/Detail.h"
#include "../Detail/GraphicsCard/GraphicsCard.h"

using namespace std;

class Utils {
public:
    static const string detailsFileName;

    static void CreateTestData();
    static void deleteFile();
    static void compareDetailsByName(Detail &detailOne, Detail &detailTwo);
    static void compareDetailsByYear(Detail &detailOne, Detail &detailTwo);
    static void writeDetailToFile(Detail &detail);
    static void printDetailsFromFile();
    static vector<GraphicsCard> getDetailsArrayFromFile();
};


#endif //INC_2_UTILS_H
