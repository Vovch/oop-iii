#include <iostream>

#include "Utils/Utils.h"
#include "UserInterface/UserInterface.h"

using namespace std;

int main() {
    Utils::CreateTestData();
    UserInterface::initUI();

    return 0;
}