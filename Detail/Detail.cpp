#include <iostream>
#include "Detail.h"

using namespace std;

Detail::Detail(string type,
               string name,
               int year,
               int weight,
               int power,
               int length,
               int width):
               type(type),
               name(name),
               year(year),
               weight(weight),
               power(power),
               length(length),
               width(width) {
    cout << "Detail has been created." << endl;
}

Detail::~Detail() {
    cout << "Detail has been destroyed." << endl;
}

stringstream Detail::getFormattedParams() {
    stringstream params;

    params << "type: ";
    params << type << endl;
    params << "name: ";
    params << name << endl;
    params << "year: ";
    params << year << endl;
    params << "weight: ";
    params << weight << " g" << endl;
    params << "power consumption: ";
    params << power << " Watt" << endl;
    params << "length: ";
    params << length << " mm" << endl;
    params << "width: ";
    params << width << " mm" << endl;

    return params;
};

string Detail::getName() {
    return name;
};

int Detail::getYear() {
    return year;
};