#include <iostream>
#include <sstream>

#ifndef INC_2_PC_H
#define INC_2_PC_H

using namespace std;

class Detail {
protected:
    string type;
    string name;
    int year;
    int weight;
    int power;
    int length;
    int width;

public:
    Detail(string type,
           string name,
           int year,
           int weight,
           int power,
           int length,
           int width);
    virtual ~Detail();
    virtual stringstream getFormattedParams();
    string getName();
    int getYear();
};
#endif //INC_2_PC_H
