#ifndef INC_2_GRAPHICSCARD_H
#define INC_2_GRAPHICSCARD_H

#include <iostream>
#include <sstream>
#include <vector>

#include "./../Detail.h"

class GraphicsCard: public Detail {
protected:
    string dxStandart;
    string openGLStandart;
    string interface;
public:
    GraphicsCard(string type,
                 string name,
                 int year,
                 int weight,
                 int power,
                 int length,
                 int width,
                 string dxStandart,
                 string openGLStandart,
                 string interface);
    ~GraphicsCard();
    stringstream getFormattedParams();
    vector<string> getParameters();
    static vector<string> getParameterNames();
};


#endif //INC_2_GRAPHICSCARD_H
