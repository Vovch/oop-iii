#include <iostream>
#include <vector>
#include "GraphicsCard.h"

GraphicsCard::GraphicsCard(string type,
   string name,
   int year,
   int weight,
   int power,
   int length,
   int width,
   string dxStandart,
   string openGLStandart,
   string interface)
: Detail(type,
         name,
         year,
         weight,
         power,
         length,
         width) {
    this->dxStandart = dxStandart;
    this->openGLStandart = openGLStandart;
    this->interface = interface;

    cout << "Graphics card has been created." << endl;
};

GraphicsCard::~GraphicsCard() {
    cout << "Graphics card has been destroyed." << endl;
};

stringstream GraphicsCard::getFormattedParams() {
    stringstream allParams = Detail::getFormattedParams();
    allParams << "DirectX version: " << dxStandart << endl;
    allParams << "OpenGL: " << openGLStandart << endl;
    allParams << "Interface: " << interface << endl;

    return allParams;
}

vector<string> GraphicsCard::getParameterNames() {
    vector<string> allParameterNames = {
        "Type",
        "Name",
        "Year",
        "Weight",
        "Power",
        "Length",
        "Width",
        "DirectX version",
        "OpenGL version",
        "Interface",
    };

    return allParameterNames;
}

vector<string> GraphicsCard::getParameters() {
    vector<string> allParameters = {
       this->type,
       this->name,
       to_string(this->year),
       to_string(this->weight),
       to_string(this->power),
       to_string(this->length),
       to_string(this->width),
       this->dxStandart,
       this->openGLStandart,
       this->interface,
    };

    return allParameters;
}